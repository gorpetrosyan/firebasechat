$(document).ready(function(){
     
    var arr = []; // List of users 
   
   $(document).on('click', '#chatMinimize', function() {
    var chatbox = $(this).parents().parents().parents().parents().attr("rel") ;
    $('[rel="'+chatbox+'"] .mdl-card__supporting-text').slideToggle('slow');
    $(this).toggleClass('window-maximize');
    if($(this).hasClass('window-maximize')){
      $(this).attr('title','Maximize')
  }else{
    $(this).attr('title','Minimize')
  }
    return false;
   });
   
   
   $(document).on('click', '#chatClose', function() { 
    var chatbox = $(this).parents().parents().parents().parents().attr("rel") ;
    $('[rel="'+chatbox+'"]').remove();
    arr.splice($.inArray(chatbox, arr), 1);
    displayChatBox();
    return false;
   });
   
   $(document).on('click', '.sidebar-user-box', function() {
  //  test
  // end test
 
    var userID = $(this).attr("data-userId");
    var userGroup = $(this).attr('data-group');
    console.log(userGroup);
    var userEmail = $(this).attr('data-email');
    var username = $(this).text() ;
    var userProfPic = $(this).attr('data-img');
    
    if ($.inArray(userID, arr) != -1)
    {
        arr.splice($.inArray(userID, arr), 1);
       }
    
    arr.unshift(userID);
    chatPopup = 
    // chatPopup =  '<div class="msg_box" style="right:270px" rel="'+ userID+'">'+
    //    '<div class="msg_head">'+username +
    //    '<div class="close">x</div> </div>'+
    //    '<div class="msg_wrap"> <div class="msg_body"> <div class="msg_push"></div> </div>'+
    //    '<div class="msg_footer"><textarea class="msg_input" rows="4"></textarea></div>  </div>  </div>' ;     
      '<div id="messages-card" class="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--12-col mdl-cell--6-col-tablet mdl-cell--6-col-desktop" rel="'+userID+'">'+
      '<div class="message_header chat-header"><div class="interviwer"><div class="chat-user-img-block">'+
                      '<a href="#"><img id="currentUserPic'+userGroup+'" src="" alt="'+username+'" title="'+username+'" class="chat-user-img"></a>'+
               '<div id="center-div" title="online" class="online"><div class="bubble"><span class="bubble-outer-dot"><span class="bubble-inner-dot"></span>'+
      '</span></div></div></div><div class="chat-user-info-block"><div class="chat-user-name-block">'+
                          '<a href="#" title=""><span class="chat-user-name" id="chatUserName'+userGroup+'"></span></a></div></div> </div>'+
          '<div class="chat-options"><div class="chat-options-minimize"><span class="window-minimize" title="Minimize" id="chatMinimize"></span>'+
              '</div><div class="chat-options-close"><i class="fa fa-times-circle" aria-hidden="true" title="Close" id="chatClose"></i></div></div></div>'+
     '<div class="mdl-card__supporting-text mdl-color-text--grey-600"><div id="messages'+userGroup+'" class="message_block"><span id="message-filler'+userGroup+'"></span></div>'+
      '<div class="chat-footer"><form id="message-form'+userGroup+'" class="message-form" action="#"><div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">'+
          '<textarea class="mdl-textfield__input" type="text" id="message'+userGroup+'" placeholder="Type message here"></textarea></div>'+
        '<button id="'+userGroup+'" disabled type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect">Send</button></form>'+
      '<form id="image-form'+userGroup+'" class="image-form" action="#"><input id="mediaCapture'+userGroup+'" class="mediaCapture" type="file" accept="image/*" capture="camera">'+
        '<button id="submitImage'+userGroup+'" title="Add an image" class="mdl-button submitImage mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--amber-400 mdl-color-text--white">'+
        '<i class="material-icons">image</i></button></form></div></div></div>';
       $("body").append(chatPopup);
    displayChatBox();
    selectChatInfo(userGroup,userProfPic,username);
   });
//    $('#message').on('input',function(){
//     var msg = $(this).val();
//     if(msg.trim().length != 0){
//     $('#submit').prop('disabled', false);
//     }
// })
   
   
//    $('#message').on('input', function(e) {    
//         //   if (e.keyCode == 13 ) {   
//               var msg = $(this).val();  
//      $(this).val('');
//      if(msg.trim().length != 0){ 
//     //  var chatbox = $(this).parents().parents().parents().attr("rel") ;
//     //  $('<div class="msg-right">'+msg+'</div>').insertBefore('[rel="'+chatbox+'"] .msg_push');
//     //  $('.msg_body').scrollTop($('.msg_body')[0].scrollHeight);
//     //  }
//           }
//       });
   
    
      
   function displayChatBox(){ 
       i = 170 ; // start position
    j = 290;  //next position
    
    $.each( arr, function( index, value ) {  
       if(index < 3){
            $('[rel="'+value+'"]').css("right",i);
      $('[rel="'+value+'"]').show();
         i = i+j;    
       }
       else{
      $('[rel="'+value+'"]').hide();
       }
          });  
   }  
   
  });